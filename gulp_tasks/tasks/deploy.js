var gulp = require('gulp');
var jade = require('gulp-jade');
var plumber = require('gulp-plumber');
var	runSequence = require('run-sequence');
var config = require('../config');
var ftpParams;
var ftp;

try {
	ftpParams = require('../server');
	ftp = require( 'vinyl-ftp' );
} catch (e) {
	ftpParams = false;
	// console.log('deploy server params unavailable');
}

var paths = config.paths;
var localPaths = config.localPaths;

gulp.task('jade:deploy', function() {
	return gulp.src([paths.pages + '*.jade'])
		.pipe(jade({
			pretty: true,
			locals: {isDeploy: true}
		}))
		.pipe(gulp.dest(paths.base));
});

gulp.task('deploy:external', function() {
	if(ftpParams === false) return;
	var conn = ftp.create(ftpParams.credentials);
	console.log('deploy start',[
		localPaths.img + '**/*',
		localPaths.js + '**/*',
		localPaths.css + '**/*',
		localPaths.fonts + '**/*',
		localPaths.base + '*.html',
		localPaths.base + 'robots.txt'
	]);

	gulp.src([
		localPaths.img + '**/*',
		localPaths.js + '**/*',
		localPaths.css + '**/*',
		localPaths.fonts + '**/*',
		localPaths.base + '*.html',
		localPaths.base + 'robots.txt'
	], { base: './', buffer: false})
	.pipe(conn.dest(ftpParams.path));
});

gulp.task('deploy', function() {
	return runSequence(
		'concat',
		['jade:deploy', 'sass'],
		'deploy:external'
	);
});

